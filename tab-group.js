export default class TabGroup extends HTMLElement {
  connectedCallback(){
    const links = this.querySelectorAll('[data-tab-link]')
    const tabs  = this.querySelectorAll('[data-tab-content]')
    const hrefs = Array.from(links, link => link.hash)

    const query = () => 
      hrefs.indexOf(location.hash)
    
    const navigate = (target) => {
      hrefs.forEach(({}, index) => {
        links[index].dataset.tabActive = target == index
         tabs[index].hidden            = target != index
      })
    }

    navigate(
      Math.max(0, query())
    )

    addEventListener('hashchange', e => {
      const index = query()

      if(index < 0)
        return
      
      e.preventDefault()
      
      navigate(index)
    })
  }
}